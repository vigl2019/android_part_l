package com.example.android_part_l;

/*

1. Создать приложение, реагирующее на перевод смартфона в airplane mode.
Когда на телефоне отключен режим airplane mode - в status bar'e должен висеть notification с текстом "Большой брат следит за тобой".
[удалить notification можно методом notificationManager.cancel(you_notify_id)]

1.1*. Улучшить приложение, добавив на экран activity статистику включенных передатчиков данных.
Например: wifi - on; bluetooth - on, gsm - on и пр.
Менять текст в notification в зависимости от включенных передатчиков
(например, если включен только gsm - "Большой брат подглядывает за тобой", если включен еще один модуль - "Большой брат присматривает за тобой" и пр.)

1.2** Добавить для разных notification'ов разные отличительные характеристики на ваше усмотрение (например это может быть вибрация, звуки, иконки и пр.).
Каждый notification должен быть уникален.

*/

import android.os.Bundle;
import android.provider.Settings;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.example.android_part_l.utils.Helper;
import com.example.android_part_l.utils.IHelper;

public class MainActivity extends AppCompatActivity {

    MyBroadcastReceiver onNotice = new MyBroadcastReceiver();
    private static final String CHANNEL_ID = "1";
    private static final int PENDING_INTENT_ID = 15;
    private static final int NOTIFY_ID = 150;
    long[] vibrationPattern = {0, 100, 1000, 300, 200, 100, 500, 200, 100};
    NotificationCompat.Builder builder = null;
    Helper helper;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        helper = new Helper(MainActivity.this);
        builder = helper.getNotificationBuilder();

        statistics();
    }

    private void statistics(){

//      String stat = "";

        if (Settings.System.getInt(MainActivity.this.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 0) {

//           stat += "AIRPLANE MODE Off" + ", ";

            helper.createNotification(getResources().getString(R.string.big_brother_title), getResources().getString(R.string.big_brother_message_1), IHelper.AIRPLANE_MODE, builder);
        } else {

//          stat += "AIRPLANE MODE On" + ", ";

            helper.createNotification(getResources().getString(R.string.big_brother_title), getResources().getString(R.string.big_brother_message_12), IHelper.AIRPLANE_MODE, builder);
        }

        if (Settings.System.getInt(MainActivity.this.getContentResolver(), Settings.Global.WIFI_ON, 0) == 0) {

//          stat += "WIFI Off" + ", ";

            helper.createNotification(getResources().getString(R.string.big_brother_title), getResources().getString(R.string.big_brother_message_21), IHelper.WIFI_MODE, builder);
        }
        else{

//          stat += "WIFI On" + ", ";

            helper.createNotification(getResources().getString(R.string.big_brother_title), getResources().getString(R.string.big_brother_message_2), IHelper.WIFI_MODE, builder);
        }

        if (Settings.System.getInt(MainActivity.this.getContentResolver(), Settings.Global.BLUETOOTH_ON, 0) == 0) {

            helper.createNotification(getResources().getString(R.string.big_brother_title), getResources().getString(R.string.big_brother_message_31), IHelper.BLUETOOTH_MODE, builder);

//          stat += "BLUETOOTH Off";
        }
        else{

//          stat += "BLUETOOTH On";

            helper.createNotification(getResources().getString(R.string.big_brother_title), getResources().getString(R.string.big_brother_message_3), IHelper.BLUETOOTH_MODE, builder);
        }
    }
}


