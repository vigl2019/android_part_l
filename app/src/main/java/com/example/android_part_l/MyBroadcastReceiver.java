package com.example.android_part_l;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import com.example.android_part_l.utils.Helper;
import com.example.android_part_l.utils.IHelper;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Helper helper = new Helper(context);
        NotificationCompat.Builder builder = helper.getNotificationBuilder();

        if(Settings.System.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0)== 0)
        {
            helper.createNotification(context.getString(R.string.big_brother_title), context.getString(R.string.big_brother_message_1), IHelper.AIRPLANE_MODE, builder);
        }
        else
        {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            notificationManager.cancel(IHelper.NOTIFY_ID_1);
        }

        if (Settings.System.getInt(context.getContentResolver(), Settings.Global.WIFI_ON, 0) == 0) {
            helper.createNotification(context.getString(R.string.big_brother_title), context.getString(R.string.big_brother_message_21), IHelper.WIFI_MODE, builder);
        }
        else{

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            notificationManager.cancel(IHelper.NOTIFY_ID_2);
        }

        if (Settings.System.getInt(context.getContentResolver(), Settings.Global.BLUETOOTH_ON, 0) == 0) {
            helper.createNotification(context.getResources().getString(R.string.big_brother_title), context.getResources().getString(R.string.big_brother_message_31), IHelper.BLUETOOTH_MODE, builder);
        }
        else{
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            notificationManager.cancel(IHelper.NOTIFY_ID_3);
        }
    }
}