package com.example.android_part_l.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.example.android_part_l.MainActivity;
import com.example.android_part_l.R;

public class Helper {

    Context context;
    private long[] vibrationPattern = {0, 100, 1000, 300, 200, 100, 500, 200, 100};

    public Helper(Context context) {
        this.context = context;
    }

    public NotificationCompat.Builder getNotificationBuilder() {

        NotificationCompat.Builder builder = null;

        // Check Android version
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { // for Oreo and above
            // create the channel
            NotificationChannel channel = new NotificationChannel(IHelper.CHANNEL_ID, "New Channel # 1", NotificationManager.IMPORTANCE_HIGH); // Title of channel "New Channel # 1" can see user in settings
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            channel.setShowBadge(true);
            channel.setVibrationPattern(vibrationPattern);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }

        // Create notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(context, IHelper.CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(context);
        }

        return builder;
    }

    public void createNotification(String notificationTitle, String notificationText, String modeType, NotificationCompat.Builder builder) {

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, IHelper.PENDING_INTENT_ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        switch (modeType.trim()) {

            case IHelper.AIRPLANE_MODE: {

                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setVibrate(vibrationPattern)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText);

                Notification notification = builder.build();
                notificationManager.notify(IHelper.NOTIFY_ID_1, notification);

                break;
            }

            case IHelper.WIFI_MODE: {

                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText);

                Notification notification = builder.build();
                notificationManager.notify(IHelper.NOTIFY_ID_2, notification);

                break;
            }

            case IHelper.BLUETOOTH_MODE: {

                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLights(358, 500, 500)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText);

                Notification notification = builder.build();
                notificationManager.notify(IHelper.NOTIFY_ID_3, notification);

                break;
            }

            default:
                break;
        }
    }
}


