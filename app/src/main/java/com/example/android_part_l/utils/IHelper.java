package com.example.android_part_l.utils;

public interface IHelper {

    String CHANNEL_ID = "1";
    int PENDING_INTENT_ID = 1;

    int NOTIFY_ID_1 = 1;
    int NOTIFY_ID_2 = 2;
    int NOTIFY_ID_3 = 3;

    String AIRPLANE_MODE = "AIRPLANE MODE";
    String WIFI_MODE = "WIFI_MODE";
    String BLUETOOTH_MODE = "BLUETOOTH MODE";
}
